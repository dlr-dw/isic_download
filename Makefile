pkg_name = data_download

install:
	pip install . --upgrade 

dev:
	pip install -e .[dev] --upgrade 
	pre-commit install

test:
	pytest

types:
	mypy $(pkg_name)/

coverage:
	pytest --cov-report html --cov $(pkg_name) tests/

docu:
	pip install .[docs] --upgrade
	sphinx-build -b html docs/source/ docs/build/html

template:
	rm -rf .git
	rm README.md
	touch README.md
	mv ${pkg_name} ${PACKAGE}
	find . -type f -not -path "./.git/*" | xargs sed -i 's/${pkg_name}/${PACKAGE}/g'

get_version:
	@printf "v"
	@python setup.py --version
