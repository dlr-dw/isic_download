from data_download import img_download
import os
import requests


def test_pass_download():
    # given
    isic_id_t = 'ISIC_0053464'
    folder_path_t = 'images/bcn_age_u30'

    # when 
    ts_download = img_download.download(isic_id_t,folder_path_t)
    
    # then
    assert os.path.isfile(folder_path_t+'/'+isic_id_t+'.jpg')

def test_error_download(capfd):
    # given
    isic_id_t = 'ISIC_003464'
    folder_path_t = 'images/bcn_age_u30'

    # when 
    ts_download = img_download.download(isic_id_t,folder_path_t)
    out,err=capfd.readouterr()
        
    # then
    assert out == "Error in downloading the image with image-ID: ISIC_003464\n"
