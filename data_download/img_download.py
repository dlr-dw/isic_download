from urllib3.exceptions import ReadTimeoutError
import requests
import shutil


def download(isic_id, folder_path):
    """Download individual images

    :param isic_id: Image id from ISIC archive.
    :param folder_path: Domain folder path to save the images.
    :returns: Image saved in the folder path provided.
    """

    img_json = requests.get(
        f"https://api.isic-archive.com/api/v2/images/{isic_id}", timeout=30)
    if img_json.status_code == 200:
        img_url = img_json.json()["files"]["full"]["url"]
        try:
            # Get image url
            response = requests.get(img_url, stream=True, timeout=30)
            # Check that the status code
            if response.status_code == 200:
                # Save image to file
                filename = f'{isic_id}.jpg'
                with open(folder_path+'/'+filename, 'wb') as image_file:
                    shutil.copyfileobj(response.raw, image_file)
        except ReadTimeoutError:
            print(f"Read timeout error image with ISIC ID: {isic_id}")
    else:
        print(f"Error in downloading the image with image-ID: {isic_id}")
