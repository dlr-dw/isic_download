import os
import pandas as pd
import img_download
import argparse
from tqdm import tqdm


def main(output_path):
    """Downloading images from domain shifted datasets in ISIC archive

    :param outputpath: Parent directory path to download images.
    :returns: Images downloaded in individual domain folders under parent directory.
    """

    metadata_path = 'metadata/'
    file_list = (os.listdir(metadata_path))
    print("Downloading datasets")
    for f in file_list:
        file_path = os.path.join(metadata_path, f)
        image_path = os.path.join(output_path, f.split('.csv')[0])

        if os.path.exists(output_path):
            print("Parent folder exists already")
        else:
            os.mkdir(output_path)

        if os.path.exists(image_path):
            print("Domain dataset folder exists already")
        else:
            os.mkdir(image_path)

        image_list = pd.read_csv(file_path)
        print(f"Downloading {f} dataset")
        for img_id in tqdm(image_list.image_id):
            if not os.path.isfile(image_path+'/'+img_id+'.jpg'):
                img_download.download(img_id, image_path)
        print(f"Completed downloading {f} dataset")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download images of domains')
    parser.add_argument('--out', metavar='output_path', required=True,
                        default='images/', help='images/')

    args = parser.parse_args()
    main(output_path=args.out)
