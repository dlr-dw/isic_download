#!/usr/bin/env python
from distutils import util
from typing import Any

from setuptools import find_packages, setup  # type: ignore[import]


pkg_name = "data_download"
author = "Sireesha Chamarthi"
author_email = "Sireesha.Chamarthi@dlr.de"
description = ""
license = "Apache-2.0"
install_requires: list[str] = [
    "pandas==1.5.0",
    "tqdm==4.64.0",
 
    ]

main_ns: dict[str, Any] = {}
ver_path = util.convert_path(f"{pkg_name}/_version.py")

with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

setup(
    name=pkg_name,
    version=main_ns["__version__"],
    python_requires=">=3.9",
    packages=find_packages(),
    include_package_data=True,
    setup_requires=[],
    install_requires=install_requires,
    extras_require={
        "dev": [
            "pre-commit==2.15.0",
            "pytest==6.2.5",
            "pytest-cov==3.0.0",
            "mypy==0.910",
        ],
        "docs": [
            "sphinx==4.2.0",
            "sphinx-rtd-theme==1.0.0",
            "sphinx-autodoc-typehints==1.12.0",
        ],
    },
    author=author,
    author_email=author_email,
    description=description,
    license=license,
)
