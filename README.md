# Downloading domain shifted datasets from ISIC archive

This repository is for downloading domain shifted dermoscopic datasets (images only) from ISIC archive ([ISIC archive website](https://www.isic-archive.com/#!/topWithHeader/wideContentTop/main)). <br>

These datasets are grouped and downloaded based on specific factors as clinic, patient age and lesion localization. The default patient we consider is from the HAM dataset, older than 30 years old and has its lesion localization on the torso or the extremities. All other datasets are considered domain shifted.

### HAM
dataset HAM10k ([The HAM10000 dataset, a large collection of multi-source dermatoscopic images of common pigmented skin lesions](https://www.nature.com/articles/sdata2018161)) <br>
clinics: Medical University of Vienna (Austria) and the skin cancer practice of Cliff Rosendahl in Queensland (Australia)
- _ham_train.csv_ and _ham_val_holdout.csv_: training (80%) and validation (20%) set by using a default patient (age>30 and loc.=torso/extremities)
- _ham_age_u30.csv_: domain shifted test set with patients <= 30 years and loc.=torso/extremities
- _ham_loc_head_neck.csv_: domain shifted test set with patients > 30 years and loc.=head/neck
- _ham_loc_palms_soles.csv_: domain shifted test set with patients > 30 years and loc.=palms/soles

### BCN
dataset BCN10k ([BCN20000: Dermoscopic lesions in the wild](https://arxiv.org/pdf/1908.02288.pdf))<br>
clinics: Hospital Clínic in Barcelona (Spain)
- _bcn_default.csv_: domain shifted test set with patients > 30 years and loc.=torso/extremities
- _bcn_age_u30.csv_: domain shifted test set with patients <= 30 years and loc.=torso/extremities
- _bcn_loc_head_neck.csv_: domain shifted test set with patients > 30 years and loc.=head/neck
- _bcn_loc_palms_soles.csv_: domain shifted test set with patients > 30 years and loc.=palms/soles

### MSK
combined datasets from MSKCC <br>
clinics: Memorial Sloan-Kettering Cancer Center New York (United States)
- _msk_default.csv_: domain shifted test set with patients > 30 years and loc.=torso/extremities
- _msk_age_u30.csv_: domain shifted test set with patients <= 30 years and loc.=torso/extremities
- _msk_loc_head_neck.csv_: domain shifted test set with patients > 30 years and loc.=head/neck
- _msk_loc_palms_soles.csv_: domain shifted test set with patients > 30 years and loc.=palms/soles

Each csv-file can be found in the _metadata_ folder and contains the ISIC_image_id and the class (1=melanoma, 0=nevus).

## Installation
```
This package can be installed standalone via git-clone or as a depencdency via pip.

#### To install the standalone package:

git clone https://gitlab.dlr.de/psdai/data_download.git
cd data_download
pip install . --upgrade 

#### To install using make:

make install

```

## Running the script

```
python data_download/main.py --out 'images/'

```
## Tests

```
pytest .

```

## Citation

```
tba 

```

## License

```
Apache License 2.0

```
